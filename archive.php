<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

// Get the header
get_header();

the_banner();

?>

<div id="content" class="archive">

    <div class="container">

        <div class="posts">

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'template-parts/content', get_post_format() ); ?>

                <?php endwhile; ?>

            <?php else : ?>

                <?php get_template_part( 'template-parts/content', 'none' ); ?>

            <?php endif; ?>

        </div>

        <nav class="pagination">

            <?php echo paginate_links(); ?>

        </nav>

    </div>

</div>

<?php get_footer();