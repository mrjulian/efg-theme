<?php if ( is_search() ) : ?>

<form id="searchform" method="get">
    
    <input type="search" id="search" class="searchfield" name="s" placeholder="<?php echo sprintf ( __('Suchen', 'efg_theme') ); ?>" value="<?php // the_search_query(); ?>" required/>
             
</form>

<?php endif; ?>