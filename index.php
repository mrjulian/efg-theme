<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header(); ?>

<div id="content" class="index">
    
    <?php the_banner(); ?>

    <div class="grid">

        <div class="container">
        
            <div class="row">

                <?php if ( have_posts() ) :

                    while ( have_posts() ) : the_post();

                    get_template_part( '/template-parts/content', efg_get_post_format() );    

                    endwhile;

                else : 

                    get_template_part( 'template-parts/content', 'none' );

                endif; ?>

            </div>

            <?php

            // Do not show the following, if page is single post
            if ( is_home() ) : ?>

                <div class="row">

                    <nav class="pagination-wrap">

                        <?php echo paginate_links(); ?>

                    </nav>

                </div>

            <?php endif; ?>

        </div>

    </div>

</div>

<?php get_footer();