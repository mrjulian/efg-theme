<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit; ?>

<article id="post-<?php the_ID(); ?>">

    <a class="card-item card-vertical" href="<?php the_permalink(); ?>">

        <div class="card-image">

            <?php if ( has_post_thumbnail() ) : ?>

                <?php the_post_thumbnail( 'medium' ); ?>

            <?php endif; ?>

        </div>

        <div class="card-content">

            <span class="card-title"><?php the_title(); ?></span>

            <div class="card-details">

                <?php the_date(); ?>

            </div>

            <div class="card-excerpt">

                <p>
                    <?php echo wp_trim_words ( get_the_content(), 20, ' ...' ); ?>
                </p>

            </div>

        </div>

    </a>

</article>