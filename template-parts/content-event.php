<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>

<article id="post-<?php echo get_the_id(); ?>">

    <a class="column-item" id="post-<?php the_ID() ?>" href="<?php the_permalink(); ?>">

        <div class="column-image">

            <?php if ( has_post_thumbnail() ) : ?>

                <?php the_post_thumbnail( 'teaser' ); ?>

            <?php endif; ?>

        </div>

        <div class="column-content">

            <span class="column-title"><?php the_title(); ?></span>

            <div class="column-details">

                <?php if ( function_exists( 'events_calendar_scheduled_details' ) ) : ?>

                <?php echo events_calendar_scheduled_details(); ?>

                <?php endif; ?>

            </div>

            <div class="column-excerpt">

                <p>
                    <?php echo wp_trim_words ( get_the_content(), 20, ' ...' ); ?>
                </p>

            </div>

        </div>

    </a>

</article>