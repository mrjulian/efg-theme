<?php
/**
 * Template Name: Leitung
 *
 * @since       1.0
 */

get_header();

the_banner();

?>

<div class="content grey">

    <div class="container">
        
        <div class="post-grid">

        <?php

        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

        $tax_query = '';

        $args = array (
            'post_type'      => 'person',
            'order'          => 'ASC',
            'orderby'        => 'menu_order',
            'posts_per_page' => 40,
        );

        $leaders = new WP_Query( $args );

        if ( $leaders->have_posts() ) {

            while ( $leaders->have_posts() ) {

                $leaders->the_post();

                get_template_part( 'template-parts/content', 'person' );
            }
        }

        else {

            echo 'Sorry, nothing found.';
        }

        ?>
            
        </div>

    </div>

</div>

<?php get_footer(); ?>