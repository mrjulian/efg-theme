<?php
/**
 * Template Name: Predigten
 *
 * @since       1.0
 */

get_header();

the_banner();

?>

<div class="content">

    <div class="container">
        
        <div id="posts" class="post-grid">

            <?php
            
            global $events;
            global $paged;
            
            // Get current page number
            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

            // Setup query arguments
            $query_args = array(
                'post_type'      => 'sermon',
                'paged'          => $paged,
                'posts_per_page' => $postnum
            );

            // Run the query
            $sermons = new WP_Query( $query_args );

            // When something has been found
            if ( $sermons->have_posts() ) {

                while ( $sermons->have_posts() ) {

                    $sermons->the_post();

                    get_template_part( 'template-parts/content', 'sermon' );
                }
            }
            else {

                return 'Wir konnten leider nichts finden.';
            }
            
            wp_reset_postdata();
            
            ?>
            
        </div>
        
        <div class="pagination">
            
            <?php
            
            if ( $sermons ) {
                
                // Get max page numbers
                $total = isset ( $sermons->max_num_pages ) ? $sermons->max_num_pages : 1;
                
                // Setup pagination arguments
                $pagination_args = array (
                    'total'     => $total,
                    'current'   => $paged,
                    'prev_next' => true,
                    'prev_text' => __('Prev', 'efg'),
                    'next_text' => __('Next', 'efg'),
                    'type'      => 'plain',
                );
                
                echo paginate_links( $pagination_args );
            }
            
            ?>
            
        </div>

    </div>

</div>

<?php get_footer(); ?>