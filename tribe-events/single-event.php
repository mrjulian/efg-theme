<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit; ?>

    <article id="post-<?php the_ID() ?>">

        <div class="container">

            <div class="event-grid">

                <div class="main">

                    <div class="single-header">

                        <?php the_title( '<h2 class="single-title">', '</h2>' ); ?>

                        <div class="single-details">

                            <?php echo events_calendar_scheduled_details(); ?>

                        </div>

                    </div>

                    <div class="single-content">

                        <?php while ( have_posts() ) : the_post(); ?>

                        <?php the_content(); ?>

                        <?php endwhile; ?>

                    </div>

                    <div class="single-actions">

                        <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

                    </div>

                </div>

                <div class="aside">

                    <div class="event-details">

                        <?php if (tribe_get_custom_field('Anmeldeformular')) : ?>

                        <div class="event-widget event-registration">

                            <a class="button button-colored" href="<?php tribe_custom_field('Anmeldeformular'); ?>" target="_blank">Anmelden</a>

                        </div>

                        <?php endif; ?>

                        <div class="event-widget event-prices">

                            <h3 class="widget-title">Eintritt</h3>

                            <div class="event-price">
                                <?php echo tribe_get_cost(get_the_id(), true); ?>
                            </div>

                        </div>

                        <div class="event-widget event-location">

                            <?php if ( events_calendar_scheduled_location() ) : ?>

                            <h3 class="widget-title">Adresse</h3>

                            <div class="event-location">

                                <?php echo events_calendar_scheduled_location(); ?>

                            </div>

                            <?php endif; ?>

                            <?php if ( events_calendar_directions_link() ) : ?>

                            <div class="event-direction">

                                <?php // echo events_calendar_directions_link(); ?>

                            </div>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </article>