<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

function display_recurring_events ( $events ) {
    
    /* */
    $last_processed = '';
    
    /* Starting output */
    $output  = '<div class="appointments">';
    
    foreach ($events as $event) {
        
        /* Save current day */
        $day = substr( $event->EventStartDate, 0, 10);
        
        /* Close previous day if exist */
        if ( ! empty ( $last_processed) && $last_processed != $day)
            $output .= '</div></div>';
        
        /* Open new day */
        if ( $last_processed != $day ) {
            
            $output .= '<div class="event-day">';
            $output .= '<div class="event-date">';
            $output .= '<div class="day">'. tribe_get_start_date($event, false, 'l') . '</div>';
            $output .= '<div class="date">' . tribe_get_start_date($event, false, 'd. F') . '</div>';
            $output .= '</div>';
            $output .= '<div class="event-holder">';
        }
        
        $output .= '<div class="single-event" id="event-' . $event->ID . '">';
        
        /* Return event start time */
        $output .= '<div class="event-time">' . tribe_get_start_time($event) . '</div>';
        
        $output .= '<div class="event-details">';
        
        /* Return event title */
        $output .= '<div class="event-title">' . get_the_title($event->ID) . '</div>';
        
        /* Return event location */
        if ( tribe_get_venue($event->ID) ) {        
            $output .= '<div class="event-venue">' . tribe_get_venue($event->ID) . '</div>';
        }
        
        $output .= '</div>';
        
        /* Return appointment end time */
        // $output .= '<div class="event-endtime">' . tribe_get_end_time($event) . '</div>';
        
        $output .= '</div>';
        
        /* Save current day to last processed day */
        $last_processed = $day;
    }
    
    $output .= '</div></div></div>';
    
    return $output;
}

function get_recurring_events ( $week = 0 ) {
    
    /* Check if events class exists */
	if ( ! class_exists( 'Tribe__Events__Main' ) )
		return;
    
    /* Define appointment date to search */
    if ( $week == 0) {
        
        $monday    = date ( 'Y-m-d', strtotime ( 'monday this week' ) );
        $tuesday   = date ( 'Y-m-d', strtotime ( 'tuesday this week' ) );
        $wednesday = date ( 'Y-m-d', strtotime ( 'wednesday this week' ) );
        $thursday  = date ( 'Y-m-d', strtotime ( 'thursday this week' ) );
        $friday    = date ( 'Y-m-d', strtotime ( 'friday this week' ) );
        $saturday  = date ( 'Y-m-d', strtotime ( 'saturday this week' ) );
        $sunday    = date ( 'Y-m-d', strtotime ( 'sunday this week' ) );
    }
    
    if ( $week == 1 ) {
        
        $monday    = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'monday this week' ) ) );
        $tuesday   = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'tuesday this week' ) ) );
        $wednesday = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'wednesday this week' ) ) );
        $thursday  = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'thursday this week' ) ) );
        $friday    = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'friday this week' ) ) );
        $saturday  = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'saturday this week' ) ) );
        $sunday    = date ( 'Y-m-d', strtotime ( '+7 day', strtotime ( 'sunday this week' ) ) );
    }
    
    if ( $week == 2 ) {
        $monday    = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'monday this week' ) ) );
        $tuesday   = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'tuesday this week' ) ) );
        $wednesday = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'wednesday this week' ) ) );
        $thursday  = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'thursday this week' ) ) );
        $friday    = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'friday this week' ) ) );
        $saturday  = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'saturday this week' ) ) );
        $sunday    = date ( 'Y-m-d', strtotime ( '+14 day', strtotime ( 'sunday this week' ) ) );
    }
    
    if ( $week == 3 ) {
        $monday    = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'monday this week' ) ) );
        $tuesday   = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'tuesday this week' ) ) );
        $wednesday = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'wednesday this week' ) ) );
        $thursday  = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'thursday this week' ) ) );
        $friday    = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'friday this week' ) ) );
        $saturday  = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'saturday this week' ) ) );
        $sunday    = date ( 'Y-m-d', strtotime ( '+21 day', strtotime ( 'sunday this week' ) ) );
    }
    
    /* Define events as global for later usage */
    global $events;
    
    /* Get current page number */
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $events = tribe_get_events( array(
        'posts_per_page' => 50,
        'start_date'     => $monday . '00:00',
        'end_date'       => $sunday . '23:59',
        'tax_query'      => array(
            array(
                'taxonomy' => 'tribe_events_cat',
                'field' => 'slug',
                'terms' => 'recurring'
            )
        ),
        'orderby'        => 'EventStartDate',
		'order'          => 'ASC'
    ) );
    
    /* Reset query */
    wp_reset_query();
    
    if ($events) {
        
        return display_recurring_events($events);
    }
    else {
        
        return 'Für diese Woche konnten wir keinen Termin finden ...';
    }
}