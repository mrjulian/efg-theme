<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

?>

<article id="post-<?php echo get_the_id(); ?>">

    <a class="column-item" id="post-<?php the_ID() ?>" href="<?php the_permalink(); ?>">

        <div class="column-image">

            <?php if ( has_post_thumbnail() ) : ?>

                <?php the_post_thumbnail( 'teaser' ); ?>

            <?php endif; ?>

        </div>

        <div class="column-content">

            <span class="column-title"><?php the_title(); ?></span>

            <div class="column-details">

                <?php echo get_the_sermon_date(); ?>

            </div>

            <div class="column-excerpt">

                <p>
                    <?php echo wp_trim_words ( get_the_content(), 20, ' ...' ); ?>
                </p>

            </div>

        </div>

    </a>

</article>