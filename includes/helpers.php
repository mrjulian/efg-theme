<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Helpers
 *
 * This file contains functions helping display the website content.
 * The functions can be used in all theme files.
 */


/**
 * Get post format
 *
 * Return the current post format inside the loop.
 */
function efg_get_post_format() {
    
    // Check if is single
    if ( is_single() ) {
        
        $post_format = 'single';
    }
    
    // Check if is page
    elseif ( is_page() ) {
        
        $post_format = 'page';
    }
    
    // Check if is blog page
    elseif ( is_home() ) {
        
        $post_format = false;
    }
    
    // Check if is search page
    elseif ( is_search() ) {
        
        $post_format = 'search';
    }
    
    // If is something else
    else {
        
        $post_format = get_post_format();
    }
    
    // Return post format
    return $post_format;
}

/**
 * Get the link
 *
 * Rewrite passed link and returns a valid URL.
 */
function get_the_link ($url, $scheme = 'http://') {
    
    // Check, if $url is not empty
    if ( ! empty ( $url ) ) {
        
        // Remove html and php tags
        $url = strip_tags ( $url );

        // Remove whitespaces
        $url = trim ( $url );
    
        // Check if link is like scheme
        if ( strpos( $url, $scheme ) === false ) {

            // Add scheme to url
            $url = 'http://' . $url;
        }
        
        return $url;
    }
}

/**
 * The link
 *
 * Display 'get_the_link'.
 */
function the_link() {
    
    echo get_the_link ($url, $scheme = 'http://');
}

function efg_get_post_type() {
    
    // Check if is single
    if ( get_post_type() == 'post' ) {
        
        $post_type = 'Beitrag';
    }
    
    // Check if is page
    elseif ( get_post_type() == 'page' ) {
        
        $post_type = 'Seite';
    }
    
    // Check if is sermon
    elseif ( get_post_type() == 'sermon' ) {
        
        $post_type = 'Predigt';
    }
    
    // Check if is blog page
    elseif ( get_post_type() == 'person' ) {
        
        $post_type = 'Person';
    }
    
    // Return post format
    if ($post_type)
        
        return $post_type;
}

function efg_get_post_type_class() {
    
    // Check if is single
    if ( get_post_type() == 'post' ) {
        
        $post_type_class = 'post';
    }
    
    // Check if is page
    elseif ( get_post_type() == 'page' ) {
        
        $post_type_class = 'page';
    }
    
    // Check if is sermon
    elseif ( get_post_type() == 'sermon' ) {
        
        $post_type_class = 'sermon';
    }
    
    // Check if is blog page
    elseif ( get_post_type() == 'person' ) {
        
        $post_type_class = 'person';
    }
    
    // Return post format
    if ($post_type_class)
        
        return $post_type_class;
}

function efg_sanitize_content ( $text, $expression ) {
 
    $output = preg_replace( $expression, '', $text);
    
    return $output;
}