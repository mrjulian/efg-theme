<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

if ( is_sermon() ) : ?>

<div class="column col-1 col_1_of_1">

    <div class="column-item" id="post-<?php the_ID() ?>">

        <div class="column-image">

            <?php if ( has_post_thumbnail() ) : ?>

                <?php the_post_thumbnail( 'medium' ); ?>

            <?php endif; ?>

        </div>

        <div class="column-content">
            
            <span class="column-title"><?php the_title(); ?></span>
            
            <div class="column-details">
                
                <span class="post-type <?php echo efg_get_post_type_class(); ?>">
                
                    <?php echo efg_get_post_type(); ?>
                
                </span>
                
                <span class="post-date">
                
                    <?php the_date(); ?>
                
                </span>

            </div>

            <div class="search-excerpt">

                <p>
                    <?php echo wp_trim_words ( efg_sanitize_content( get_the_content(), '/(\[...*])/' ), 80, ' ...' ); ?>
                </p>

            </div>

        </div>

    </div>

</div>

<?php else : ?>

    <div class="column col-1 col_1_of_1">

        <a class="column-item" id="post-<?php the_ID() ?>" href="<?php the_permalink(); ?>">

            <div class="column-image">

                <?php if ( has_post_thumbnail() ) : ?>

                    <?php the_post_thumbnail( 'medium' ); ?>

                <?php endif; ?>

            </div>

            <div class="column-content">

                <h5 class="column-title"><?php the_title(); ?></h5>

                <div class="column-details">

                    <span class="post-type <?php echo efg_get_post_type_class(); ?>">

                        <?php echo efg_get_post_type(); ?>

                    </span>

                    <?php if ( ! ( get_post_type() == 'page' ) ) : ?>
                    
                        <span class="post-date">

                            <?php the_date(); ?>

                        </span>
                    
                    <?php endif; ?>

                </div>

                <div class="search-excerpt">

                    <p>
                        <?php echo wp_trim_words ( efg_sanitize_content( get_the_content(), '/(\[...*])/' ), 80, ' ...' ); ?>
                    </p>

                </div>

            </div>

        </a>

    </div>

<?php endif; ?>