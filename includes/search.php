<?php

function efg_get_search_result_details( $wp_query ) {
    
    // Check if count is is equal to 1. If true, return 'Ergebnis' instead 'Ergebisse'
    if ( $wp_query->found_posts == 1 ) {
    
        $output  = '<span class="medium result-count">' . $wp_query->found_posts . '</span><span> Ergebnis für </span><span class="medium search-query">"' . get_search_query() . '"</span>';
    }
    else {
        
        $output  = '<span class="medium">' . $wp_query->found_posts . '</span><span> Ergebnisse für </span><span class="medium">"' . get_search_query() . '"</span>';
    }

    // Return only if not empty
    if ( ! empty ( $output ) )
        
        return $output;
}

// Exclude pages from search
function efg_remove_pages_from_search() {
    
    global $wp_post_types;
    
    $wp_post_types['page']->exclude_from_search = true;
}

add_action('init', 'efg_remove_pages_from_search');