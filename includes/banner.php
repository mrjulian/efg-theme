<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Get the banner
 *
 * Displays banner and title on pages and the blog.
 * If is post, only the banner is displayed.
 */

function get_the_banner() {
    
    // Blog page
    if ( is_home() ) {
        
        // Get the thumbnail ID of the blog page showing the posts
        $large_banner_id = get_post_thumbnail_id ( get_option ( 'page_for_posts', true ) );

        // Get the thumbnail URL
        $large_banner_url_small = wp_get_attachment_image_src ( $large_banner_id, 'banner' );

        // Save thumbnail as background-style
        $large_banner = esc_url( $large_banner_url[0] );
        
        // Get the title of the blog page showing the posts
        $title = get_the_title( get_option ('page_for_posts', true) );
    }
    
    // Page
    elseif ( is_page() ) {
        
        // Get the thumbnail ID of the displayed page
        $large_banner_id = get_post_thumbnail_id();
        
        // Get the thumbnail URL
        $large_banner_url = wp_get_attachment_image_src( $large_banner_id, 'banner' );
        
        // Save thumbnail as background-style
        $large_banner = esc_url( $large_banner_url[0] );
        
        // Get the title of the displayed page
        $title = get_the_title();
    }
    
    // Current page is the archive
    elseif ( is_archive() ) {
        
        if ( is_archive ( 'sermon' ) ) {
            
            // Get background image from theme settings
            $large_banner = get_option('sermon_archive_banner');
        }
        else {
            
            // Get background image from theme settings
            $large_banner = get_option('general_archive_banner');
        }
        
        // Get the title
        $title = get_the_archive_title();
    }
    
    // Current page displays search results
    elseif ( is_search() ) {
        
        // Get background image from theme settings
        $large_banner = get_option('general_search_banner');

        // Display the search form
        $title = get_search_form( false );
    }
    
    else {

        // Get the thumbnail ID
        $large_banner_id = get_post_thumbnail_id();
        
        // Get the thumbnail URL
        $large_banner_url = wp_get_attachment_image_src( $large_banner_id, 'banner' );
        
        // Save image as background-style
        $large_banner = esc_url( $large_banner_url[0] );
    }
    
    /**
     * If page or post has an image, display the banner
     */

    if ( isset ( $large_banner ) ) {
        
        // Get the mobile banner
        $small_banner = MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), "mobile-banner" );
        
        // Open output var
        $output  = '<div class="banner">';
        
        // Add the banner shadow
        $output .= '<div class="banner-shadow"></div>';
        
        // Add the mobile banner if present
        $output .= ( $small_banner ) ? '<img class="banner-image banner-image-small" src="' . $small_banner . '">' : '';
        
        // Add the banner image selected above
        $output .= '<img class="banner-image banner-image-large" src="' . $large_banner . '">';
        
        // Add the title if present
        $output .= ( $title ) ? '<h1 class="banner-title light-color">' . $title . '</h1>' : ''; 
        
        // Close output var
        $output .= '</div>';
        
        // Return generated output
        return $output;
    }
}

/**
 * The banner
 *
 * Displays banner and title on pages and the blog.
 * If is post, only the banner is displayed.
 */

function the_banner() {
    
    echo get_the_banner();
}