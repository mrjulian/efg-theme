<article id="post-<?php echo get_the_id(); ?>">
    
    <div class="column-item leader-card" id="post-<?php echo get_the_id(); ?>">
        
        <div class="column-image">
            
            <?php
            
            if ( has_post_thumbnail () ) {
            
                echo get_the_post_thumbnail();
            }
            
            ?>
            
        </div>
        
        <div class="column-content">
            
            <h4 class="column-title"><?php echo get_the_title(); ?></h4>
            
            <div class="column-details">
                
                <p><?php echo get_the_person_position(); ?></p>
                
            </div>
            
        </div>
        
    </div>
    
</article>