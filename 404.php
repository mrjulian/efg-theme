<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header(); ?>

<div id="404" class="notfound">

    <div class="row container">
        
        <div class="error-wrapper">
            
            <div class="container">
        
                <h1 class="error-title">Nichts gefunden ...</h1>

                <p class="error-subtitle">Leider kann die Seite nicht angezeigt werden.</p>
                
                <a href="<?php echo get_site_url(); ?>" class="button button-medium button-lucent">Startseite</a>
                
            </div>
        
        </div>

    </div>

</div>