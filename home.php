<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header(); ?>

<div id="content" class="home">
    
    <?php the_banner(); ?>

    <div class="grid">
        
        <div class="container">

            <div class="row">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'template-parts/content' ); ?>

                    <?php endwhile; ?>

                <?php else : ?>

                    <?php get_template_part( 'template-parts/content', 'none' ); ?>

                <?php endif; ?>

            </div>

            <div class="row">

                <nav class="pagination-wrap">

                <?php echo paginate_links(); ?>

                </nav>

            </div>

        </div>
        
    </div>

</div>

<?php get_footer();