/**
 * fade-text.js
 *
 * Checks viewport and fade divs in.
 *
 */

var $, jQuery;

jQuery(function ($) {

    'use strict';

    $('.animate').addClass("hidden").viewportChecker({

        classToAdd: 'visible animated fadeInUp',
        offset: 0
    });

    $('.card-header').click(function () {

        var par = $(this).parents('.card');

        par.find('.card-toggle').toggleClass('active');
        par.find('#collapse').collapse('toggle');

        return false;
    });
});