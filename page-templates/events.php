<?php
/**
* Template Name: Veranstaltungen
*
* @since       1.0
*/

get_header();

the_banner();

?>

    <div class="content grey">

        <ul class="nav-tabs" role="tablist">

            <li role="presentation" class="first active"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Veranstaltungen</a></li>

            <li role="presentation" class="second"><a href="#appointments" aria-controls="appointments" role="tab" data-toggle="tab">Termine</a></li>

        </ul>

        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="events">

                <div class="container">

                    <div class="post-grid">

                        <?php

                        // Prüfen ob Klasse "Tribe__Events__Main" geladen ist!
                        if ( ! class_exists( 'Tribe__Events__Main' ) )
                            return;

                        global $events;
                        global $paged;

                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

                        $args = array (
                            'post_type'      => Tribe__Events__Main::POSTTYPE,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order',
                            'paged'          => $paged,
                            'posts_per_page' => get_option('posts_per_event_page'),
                            'tax_query'      => array(
                                array(
                                    'taxonomy' => 'tribe_events_cat',
                                    'field' => 'slug',
                                    'terms' => 'recurring',
                                    'operator' => 'NOT IN'
                                )
                            )
                        );

                        $events = new WP_Query( $args );

                        if ( $events->have_posts() ) {

                            while ( $events->have_posts() ) {

                                $events->the_post();

                                require get_template_directory() . '/template-parts/content-event.php';
                            }
                        }
                        
                        wp_reset_query();

                        ?>

                    </div>

                    <div class="pagination">

                        <?php

                        $total = isset ( $events->max_num_pages ) ? $events->max_num_pages : 1;

                        $pagination_args = array(
                            'total'     => $total,
                            'current'   => $paged,
                            'prev_next' => true,
                            'prev_text' => __('Zurück', 'efg'),
                            'next_text' => __('Vorwärts', 'efg'),
                            'type'      => 'plain',
                        );

                        echo paginate_links( $pagination_args );

                        ?>

                    </div>

                </div>

            </div>

            <div role="tabpanel" class="tab-pane face" id="appointments">

                <div class="container">

                    <div id="appointment-carousel" class="carousel slide" data-ride="carousel">

                        <div class="carousel-inner" role="listbox">

                            <div class="item active">

                                <h2 class="week-title">Diese Woche</h2>

                                <?php echo get_recurring_events(); ?>

                            </div>

                            <div class="item">

                                <h2 class="week-title">Nächste Woche</h2>

                                <?php echo get_recurring_events(1); ?>

                            </div>

                            <div class="item">

                                <h2 class="week-title">Übernächste Woche</h2>

                                <?php echo get_recurring_events(2); ?>

                            </div>

                        </div>

                        <a class="left carousel-control grey" href="#appointment-carousel" role="button" data-slide="prev">

                            <div class="arrow-wrap">

                                <i class="arrow arrow-left"></i>

                            </div>

                        </a>

                        <a class="right carousel-control grey" href="#appointment-carousel" role="button" data-slide="next">

                            <div class="arrow-wrap">

                                <i class="arrow arrow-right"></i>

                            </div>

                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <?php get_footer(); ?>