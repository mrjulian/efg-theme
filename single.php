<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header();

the_banner();

?>

<div class="content">

    <div class="container container-centered">

        <?php while ( have_posts() ) : the_post(); ?>

        <div class="single-header">

            <?php the_title( '<h2 class="single-title">', '</h2>' ); ?>

            <div class="single-details">

                <span class="date"><?php the_date('d. F Y'); ?></span>

            </div>

        </div>

        <div class="single-content">

            <?php the_content(); ?>

        </div>

        <?php endwhile; ?>

    </div>

</div>

<?php get_footer();