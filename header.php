<?php if ( ! defined ( 'ABSPATH' ) ) exit; ?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <style type="text/css">
        <?php echo str_replace(' ', '', get_option('theme-style'));
        ?>
    </style>

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <div id="page" class="menu-closed">

        <div class="shadow"></div>

        <div class="navbar">

            <div class="container bottom-line">

                <div class="left">

                    <div class="branding">

                        <a class="page-logo" href="<?php echo get_site_url() ?>">

                            <img class="logo-image logo-white" src="<?php echo get_site_url() . '/data/brand/logo-light-web.svg' ?>">
                            <img class="logo-image logo-dark" src="<?php echo get_site_url() . '/data/brand/logo-dark-web.svg' ?>">

                        </a>

                        <span class="page-title"><?php bloginfo( 'name' ); ?></span>

                    </div>

                </div>

                <div class="right">

                    <div class="navigation">

                        <?php

                        wp_nav_menu (
                            array (
                                'theme_location' => 'primary',
                                'menu_id'    => 'default-nav',
                                'menu_class' => 'default-nav',
                                'container'  => 'ul'
                            )
                        );

                        ?>

                    </div>

                    <div class="menu-toggle">

                        <div class="toggle-button">

                            <div class="toggle-pointer">

                                <span class="toggle-text">Menu</span>

                                <div class="toggle-icon">

                                    <span class="burger-bar burger-bar-first"></span>
                                    <span class="burger-bar burger-bar-second"></span>
                                    <span class="burger-bar burger-bar-third"></span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>