<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Get the return button
 *
 * If current page is not frontpage,
 * return a return button in the mobile header.
 */

function efg_get_the_return_button() {
    
    if ( ! is_front_page() ) {
        
        return '<a class="return-button" href="javascript:window.history.back()"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>';
    }
}

/**
 * The return button
 *
 * If current page is not frontpage,
 * displays a return button in the mobile header.
 */

function efg_the_return_button() {
    
    echo efg_get_the_return_button();
}

class efg_main_nav_walker extends Walker_Nav_Menu {
    
    function start_lvl (&$output, $depth = 0, $args = array() ) {
        
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu\">\n";
    }
    
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = '';
        
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $submenus = 0 == $depth || 1 == $depth ? get_posts( array( 'post_type' => 'nav_menu_item', 'numberposts' => 1, 'meta_query' => array( array( 'key' => '_menu_item_menu_item_parent', 'value' => $item->ID, 'fields' => 'ids' ) ) ) ) : false;
        
        if ( empty ( $submenus ) ) {
        
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        }

        $item_output = $args->before;
        $item_output .= ! $submenus ? '<a'. $attributes .'>' : '<span class="menu-item-inline"><span class="sub-menu-title"><span class="wrap">';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

            $item_output .= ! empty( $submenus ) ? ( 0 == $depth ? '</span></span><span class="sub-menu-button"><span class="wrap"><span class="plus-wrap"><span class="plus-icon horizontal-line"></span><span class="plus-icon vertical-line"></span></span></span></span>' : '<span class="sub-arrow"></div>' ) : '';
                
        $item_output .= ! $submenus ? '</a>' : '</span>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}