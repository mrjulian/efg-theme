<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/*--------------------------------------------------------------
 * Konstanten festlegen
 *------------------------------------------------------------*/

define ( 'EFG_THEME_PATH',  get_template_directory() );
define ( 'EFG_ASS_PATH',    get_template_directory() . '/assets/' );
define ( 'EFG_INC_PATH',    get_template_directory() . '/includes/' );

define ( 'EFG_THEME_URL',   get_template_directory_uri() . '/efg-content/' );
define ( 'EFG_ASS_URL',     get_template_directory_uri() . '/assets/' );
define ( 'EFG_INC_URL',     get_template_directory_uri() . '/includes/' );

/*--------------------------------------------------------------
 * Dateien einbinden
 *------------------------------------------------------------*/

function efg_load_theme_files() {
    
    // Modifizierungen für Event Calendar einbinden
    include_once EFG_THEME_PATH . '/tribe-events/tweaks.php';
    
    // Dateien vom Theme einbinden
    include_once EFG_INC_PATH . '/banner.php';
    include_once EFG_INC_PATH . '/conditions.php';
    include_once EFG_INC_PATH . '/events.php';
    include_once EFG_INC_PATH . '/helpers.php';
    include_once EFG_INC_PATH . '/navigation.php';
    include_once EFG_INC_PATH . '/pagination.php';
    include_once EFG_INC_PATH . '/search.php';
    include_once EFG_INC_PATH . '/settings.php';
    include_once EFG_INC_PATH . '/titles.php';
}

add_action( 'after_setup_theme', 'efg_load_theme_files');

/*--------------------------------------------------------------
 * Standard WordPress Funktionen aktivieren
 *------------------------------------------------------------*/

function efg_setup_wp_features() {
    
    // Navigation registrieren
    register_nav_menus ( array (
        'primary'   => esc_html__( 'Primary Menu', 'efg_theme' ),
        'secondary' => esc_html__( 'Secondary Menu', 'efg_theme' ),
    ) );

    // Festlegen, dass WordPress Seitentitel festlegt
    add_theme_support( 'title-tag' );

    // Thumbnails aktivieren
    add_theme_support( 'post-thumbnails' );

    // HTML5 Markup aktivieren
    add_theme_support( 'html5', array(
        'search-form',
        'gallery',
        'caption',
    ) );
}

add_action( 'after_setup_theme', 'efg_setup_wp_features');

/*--------------------------------------------------------------
 * Sidebars registrieren
 *------------------------------------------------------------*/

function efg_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'efg' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'efg' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'efg_widgets_init' );

/*--------------------------------------------------------------
 * Neue Bildgrößen registrieren
 *------------------------------------------------------------*/

function efg_add_image_sizes () {
    
    add_image_size( 'banner', 1920, 500, true );
    add_image_size( 'mobile', 400, 500, true );
    add_image_size( 'teaser', 300, 188, true );
}

add_action( 'after_setup_theme', 'efg_add_image_sizes' );

/*--------------------------------------------------------------
 * Frontend scripts einbetten
 *------------------------------------------------------------*/

function efg_load_theme_scripts() {
    
    wp_enqueue_script( 'jquery',          site_url() . '\wp-includes\js\jquery' );
    wp_enqueue_script( 'navigation',      EFG_ASS_URL . 'js/navigation.js' );
    wp_enqueue_script( 'animations',      EFG_ASS_URL . 'js/animations.js' );
    wp_enqueue_script( 'bootstrap',       EFG_ASS_URL . 'js/bootstrap.min.js' );
    wp_enqueue_script( 'viewportchecker', EFG_ASS_URL . 'js/viewportchecker.min.js' );
}

add_action( 'wp_enqueue_scripts', 'efg_load_theme_scripts' );

/*--------------------------------------------------------------
 * Frontend styles einbetten
 *------------------------------------------------------------*/

function efg_load_theme_style() {

    global $is_IE;
    
    // Load regular stylesheet
    wp_enqueue_style( 'theme',  EFG_ASS_URL . 'css/theme.min.css' );
    
    // Load stylesheet to support Internet Explorer
    if ($is_IE === true) {
        wp_enqueue_style( 'theme-ie', EFG_ASS_URL . 'css/ie.css', array( 'theme' )  );
    }
}

add_action( 'wp_enqueue_scripts', 'efg_load_theme_style' );

/*--------------------------------------------------------------
 * Admin styles einbetten
 *------------------------------------------------------------*/

function efg_load_admin_style() {
    
    wp_enqueue_style( 'admin-styles', EFG_ASS_URL . 'css/admin.css' );
}

add_action('admin_enqueue_scripts', 'efg_load_admin_style');

/*--------------------------------------------------------------
 * Post-Query für Archivseiten anpassen
 *------------------------------------------------------------*/

function efg_posts_per_archive_page( $query ) {
    
    if ( $query->is_archive() ) {
        
        // Get number of posts shall be displayed on archive pages
        $posts_per_page = get_option('posts_per_archive_page');
        
        // Change value in query
        set_query_var('posts_per_page', $posts_per_page );
    }
}

add_action( 'pre_get_posts', 'efg_posts_per_archive_page' );

/*--------------------------------------------------------------
 * Post-Query für Suchergebnisse anpassen
 *------------------------------------------------------------*/

function efg_posts_per_search_page( $query ) {
    
    if ( $query->is_search() ) {
        
        // Get number of posts shall be displayed on archive pages
        $posts_per_page = get_option('posts_per_search_page');
        
        // Change value in query
        set_query_var('posts_per_page', $posts_per_page );
    }
}

add_action( 'pre_get_posts', 'efg_posts_per_search_page' );

/*--------------------------------------------------------------
 * Titel auf Archivseiten anpassen
 *------------------------------------------------------------*/

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    
    // Archiv für custom post types
    elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }
  
    return $title;
}
 
add_filter( 'get_the_archive_title', 'my_theme_archive_title' );

/*--------------------------------------------------------------
 * MultiPostThumbnail aktivieren
 *------------------------------------------------------------*/

if ( class_exists ( 'MultiPostThumbnails' ) ) {
    
    $types = array('post', 'page', 'sermon', Tribe__Events__Main::POSTTYPE);
    
    foreach( $types as $type ) {
        new MultiPostThumbnails (
            array (
                'label' => 'Beitragsbild (Mobile)',
                'id' => 'mobile-banner',
                'post_type' => $type,
            )
        );
    }
}

/*--------------------------------------------------------------
 * Support für Post Tages deaktivieren
 *------------------------------------------------------------*/

function efg_remove_tags() {
    
    register_taxonomy( 'post_tag', array() );
}

add_action('init', 'efg_remove_tags');

/*--------------------------------------------------------------
 * Support für Kommentare deaktivieren
 *------------------------------------------------------------*/

function efg_disable_comments() {
    
    $post_types = get_post_types();
    
    foreach ($post_types as $post_type) {
        if ( post_type_supports ( $post_type, 'comments' ) ) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}

add_action('admin_init', 'efg_disable_comments');

/*--------------------------------------------------------------
 * Kommentarfunktion im Frontend deaktivieren
 *------------------------------------------------------------*/

function efg_disable_comments_status() {
    
    return false;
}

add_filter('comments_open', 'efg_disable_comments_status', 20, 2);
add_filter('pings_open', 'efg_disable_comments_status', 20, 2);

/*--------------------------------------------------------------
 * Kommentarfunktion im Backend deaktivieren
 *------------------------------------------------------------*/

function efg_disable_comments_admin_menu() {
    
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'efg_disable_comments_admin_menu');

/*--------------------------------------------------------------
 * Veraltete DIV Klassen in Visual Composer deaktivieren
 *------------------------------------------------------------*/

function vc_remove_deprecated_classes ( $class_string, $tag ) { 
    
    // Remove deprecated row names
    if ( strpos ( $tag, 'row') !== false) {
        
        $class_string = str_replace( 'wpb_row', '', $class_string );
    }
    
    // Remove deprecated column names
    if ( strpos ( $tag, 'column') !== false) {
        
        $class_string = str_replace( 'wpb_column', '', $class_string );
    }
    
    // Return remaining classes from array
    return $class_string;
}

add_filter( 'vc_shortcodes_css_class', 'vc_remove_deprecated_classes', 10, 2 );

/**
 * Bugfix für "ob_end_flush(): failed to send buffer of zlib output compression (1) in \efg\wp-includes\functions.php on line 3718"
 *
 * @since       1.0.0
 */

remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );