<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header();

the_banner();

?>

<div class="content">

    <div class="container container-centered">

        <?php while ( have_posts() ) : the_post(); ?>

            <span class="sermon-date"><?php the_date('d. F Y'); ?></span>

            <?php the_title( '<h2 class="sermon-title">', '</h2>' ); ?>

            <ul class="sermon-details">

                <li class="sermon-speaker">Referent: <?php echo get_the_sermon_terms('sermon_speaker'); ?></li>
                <li class="sermon-series">Reihe: <?php echo get_the_sermon_terms('sermon_series'); ?></li>
                <li class="sermon-topics">Themen: <?php echo get_the_sermon_terms('sermon_topic'); ?></li>

            </ul>

            <div class="single-content">

                <?php the_content(); ?>

            </div>

        <?php endwhile; ?>

    </div>

</div>

<?php get_footer();