<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Get the page title
 *
 * If current page is not frontpage, return the title of the page.
 */

function efg_get_the_page_title() {
        
    // Front page
    if ( is_front_page() ) {
        
        return get_bloginfo( 'name' );
    }
    
    // Blog
    elseif (is_home() ) {
        
        return 'Aktuelles';
    }
    
    // Page
    elseif ( is_page() ) {
        
        return get_the_title();
    }
    
    // Single event
    elseif ( is_singular('tribe_events') ) {
        
        return 'Termine';
    }
    
    // Single post
    elseif ( is_single()  ) {
        
        return 'Aktuelles';
    }
}

/**
 * Get the page title
 *
 * If current page is not frontpage, display the title of the page.
 */

function efg_the_page_title() {

    echo efg_get_the_page_title();
}