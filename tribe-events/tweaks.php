<?php

// Unload events calendar stylesheets for including them into sass
function events_calendar_remove_stylesheets() {

	wp_dequeue_style( 'tribe-events-full-calendar-style' );
	wp_dequeue_style( 'tribe-events-calendar-style' );
	wp_dequeue_style( 'tribe-events-calendar-full-mobile-style' );
	wp_dequeue_style( 'tribe-events-calendar-mobile-style' );
	wp_dequeue_style( 'tribe-events-full-pro-calendar-style' );
	wp_dequeue_style( 'tribe-events-calendar-pro-style' );
	wp_dequeue_style( 'tribe-events-calendar-full-pro-mobile-style' );
	wp_dequeue_style( 'tribe-events-calendar-pro-mobile-style' );
    wp_dequeue_style( 'tribe_events-widget-calendar-pro-style' );
    wp_dequeue_style( 'tribe_events-widget-calendar-pro-override-style' );
    wp_dequeue_style( 'tribe_events-widget-this-week-pro-style' );
    wp_dequeue_style( 'tribe-events-calendar-pro-override-style' );
    wp_dequeue_style( 'tribe-events-tickets-rsvp-override-style' );
    
    wp_deregister_style( 'event-tickets-rsvp' );
}

// add_action( 'wp_enqueue_scripts', 'events_calendar_remove_stylesheets', 100 );

// Unload isotope js
function events_calendar_remove_isotope() {
    
    // Remove isotope js on photo view
    if(wp_script_is('tribe-events-pro-isotope', $list = 'enqueued' )) {
        
        global $wp_scripts;
        
        // modifiyng global scripts, removing dependency for photo-view (was dependend on isotope.js)
        $wp_scripts->registered["tribe-events-pro-photo"]->deps =array();
        
        // dequeue isotope
        wp_dequeue_script('tribe-events-pro-isotope');
        wp_deregister_script('tribe-events-pro-isotope');
	}
}

add_action('wp_print_scripts', 'events_calendar_remove_isotope', 100);

// Hooks into build-in event query to hide passed events in frontend
function events_calendar_hide_passed_events ($wp_query) {

	if ( !is_admin() )  {

		$new_meta = array();
		$today = new DateTime();

		// Join with existing meta_query
        if( is_array( $wp_query->meta_query ) ) {
            
            $new_meta = $wp_query->meta_query;
        }

		// Add new meta_query, select events ending from now forward
		$new_meta[] = array(
			'key' => '_EventEndDate',
			'type' => 'DATETIME',
			'compare' => '>=',
			'value' => $today->format('Y-m-d H:i:s')
		);

		$wp_query->set( 'meta_query', $new_meta );
	}

	return $wp_query;
}

add_filter('tribe_events_pre_get_posts', 'events_calendar_hide_passed_events', 100);

function events_calendar_scheduled_details() {
    
    $post_id = get_the_ID();
    $time_format = get_option( 'time_format', Tribe__Date_Utils::TIMEFORMAT );

    $start_date = tribe_get_start_date( null, false );
    $start_time = tribe_get_start_date( null, false, $time_format );
    $end_date = tribe_get_display_end_date( null, false );
    $end_time = tribe_get_end_date( null, false, $time_format );
    
    $output = '';
    
    if (! is_single()) {
        
        if ( ($start_date ) && (! $end_date) || ($start_date == $end_date) ) {
        
            $output .= '<span class="event-scheduled-date">' . $start_date . '</span>';

            if ($start_time) {

                $output .= '<span class="event-scheduled-time">, ' . $start_time . ' Uhr</span>';
            }
        }

        elseif ( ($start_date ) && ($end_date) ) {

            $output .= '<span class="event-scheduled-date full-width">' . $start_date . ' bis ' . $end_date . '</span>';
        }
    }
    
    else {
        
        if ( ($start_date ) && (! $end_date) || ($start_date == $end_date) ) {
        
            $output .= '<span class="event-scheduled-date">' . $start_date . '</span>';

            if ($start_time) {

                $output .= '<span class="event-scheduled-time">, ' . $start_time . ' Uhr</span>';
            }
        }

        elseif ( ($start_date ) && ($end_date) ) {

            $output .= '<span class="event-scheduled-date full-width">' . $start_date . ' bis ' . $end_date . '</span>';

            if ($start_time) {

                $output .= '<span class="event-scheduled-time full-width">' . $start_time . ' Uhr</span>';
            }
        }
    }
    
    
    
    return apply_filters( 'events_calendar_scheduled_details', $output); 
}

function events_calendar_scheduled_location() {
    
    $post_id = get_the_ID();
    
    $location    = tribe_get_venue();
    $street      = tribe_get_address();
    $city        = tribe_get_city();
    $postal_code = tribe_get_zip();
    
    $output = '';
    
    if ($location) {
    
        $output .= '<span class="location-name full-width alignleft">' . $location . '</span>';
        
        if ( $street && $city && $postal_code) {
            $output .= '<span class="location-street full-width alignleft">' . $street . '</span>';
            $output .= '<span class="location-postal-code ">' . $postal_code . '</span>';
            $output .= '<span class="location-city"> ' . $city . '</span>';
        }
        
        return apply_filters( 'events_calendar_scheduled_location', $output); 
    }    
}

function events_calendar_directions_link() {
    
    $link = tribe_get_map_link();
    
    if ( $link ) {
        
        $output  = '';
        $output .= '<a class="direction-link" onclick="window.open(\'' . $link . '\', \'_blank\')">';
        $output .= 'Route planen';
        $output .= '</a>';

        return apply_filters( 'events_calendar_directions_link', $output);
    }
}

function events_calendar_registration_button() {
    
    $post_id = get_the_ID();
    
    return apply_filters( 'events_calendar_registration_button', $output); 
}

add_filter( 'tribe_events_admin_show_cost_field', '__return_true', 100 );