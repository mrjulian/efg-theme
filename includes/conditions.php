<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Conditions
 *
 * This file contains condition functions to decide if something should be displayed or not.
 * The conditions can be used in the theme files.
 */

/**
 * Is sermon
 *
 * Returns 'true' if post type is sermon.
 */
function is_sermon() {
    
    if ( get_post_type() == 'sermon' )
        
        return true;
}

/**
 * Is person
 *
 * Returns 'true' if post type is person.
 */
function is_person() {
    
    if ( get_post_type() == 'sermon' )
        
        return true;
}

/**
 * Is gprofil
 *
 * Returns 'true' if post type is gprofil.
 */
function is_gprofil() {
    
    if ( get_post_type() == 'sermon' )
        
        return true;
}