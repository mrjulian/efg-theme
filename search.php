<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header(); ?>

    <div id="content" class="search">

        <?php the_banner(); ?>

        <div class="grid">

            <div class="container">

                <div class="row" id="search-details">

                    <h3>
                        <?php echo efg_get_search_result_details( $wp_query ); ?>
                    </h3>

                </div>

                <div class="row" id="search-results">

                    <?php if ( have_posts() ) : ?>

                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php get_template_part( 'template-parts/content', efg_get_post_format() ); ?>

                        <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                    <?php endif; ?>

                </div>

                <div class="row">

                    <nav class="paging">

                        <?php echo paginate_links(); ?>

                    </nav>

                </div>

            </div>

        </div>

    </div>

    <?php get_footer();