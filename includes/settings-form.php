<div class="wrap">

    <h1>Themes › Einstellungen</h1>

    <form method="post">
        
        <?php echo wp_nonce_field( 'theme_settings_nonce' ); ?>

        <h2 class="title">Beiträge auf einer Seite</h2>

        <table class="form-table">

            <tbody>
                
                <tr>
                    <th scope="row">
                        <label for="blogname">Termine zeigen maximal</label>
                    </th>

                    <td>
                        <input name="posts_per_event_page" type="number" step="1" min="1" id="posts_per_event_page" value="<?php echo get_option('posts_per_event_page'); ?>" class="small-text"> Veranstaltungen
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="blogname">Archive zeigen</label>
                    </th>

                    <td>
                        <input name="posts_per_archive_page" type="number" step="1" min="1" id="posts_per_archive_page" value="<?php echo get_option('posts_per_archive_page'); ?>" class="small-text"> Beiträge
                    </td>
                </tr>
                
                <tr>
                    <th scope="row">
                        <label for="blogname">Suchen zeigen maximal</label>
                    </th>

                    <td>
                        <input name="posts_per_search_page" type="number" step="1" min="1" id="posts_per_search_page" value="<?php echo get_option('posts_per_search_page'); ?>" class="small-text"> Ergebnisse
                    </td>
                </tr>

            </tbody>

        </table>

        <h2 class="title">Banner</h2>

        <p>Im folgenden Abschnitt können Bilder hinterlegt werden, die als Banner auf vom System generierten Seiten angezeigt werden.</p>

        <table class="form-table">

            <tbody>
                
                <tr>
                    <th scope="row">
                        <label for="blogname">Predigtarchiv</label>
                    </th>

                    <td>
                        <input name="sermon_archive_banner" type="text" value="<?php echo get_option('sermon_archive_banner'); ?>" class="longer-text">
                    </td>

                </tr>

                <tr>
                    <th scope="row">
                        <label for="blogname">Andere Archive</label>
                    </th>

                    <td>
                        <input name="general_archive_banner" type="text" value="<?php echo get_option('general_archive_banner'); ?>" class="longer-text">
                    </td>

                </tr>

                <tr>
                    <th scope="row">
                        <label for="blogname">Suchergebnisse</label>
                    </th>

                    <td>
                        <input name="general_search_banner" type="text" value="<?php echo get_option('general_search_banner'); ?>" class="longer-text">
                    </td>

                </tr>

            </tbody>

        </table>

        <h2 class="title">Sonstiges</h2>

        <table class="form-table">

            <tbody>

                <tr>
                    <th scope="row">
                        <label for="blogname">CSS above the fold</label>
                    </th>

                    <td>
                        <textarea rows="10" cols="200" name="css_above_the_fold" id="css_above_the_fold"><?php echo get_option('css_above_the_fold'); ?></textarea>
                        <p class="description" id="tagline-description">Bitte nur <a href="https://cssminifier.com" target="_blank">komprimiertes CSS</a> verwenden.</p>
                    </td>

                </tr>

            </tbody>

        </table>

        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Änderungen übernehmen"></p>

    </form>

</div>