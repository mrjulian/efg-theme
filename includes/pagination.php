<?php

function the_sermon_pagination ( $posts, $paged ) {
    
    // Get total number of posts
    $total = isset ( $posts->max_num_pages ) ? $posts->max_num_pages : 1;
    
    $page_args = array (
        'total'     => $total,
        'current'   => $paged,
        'prev_next' => true,
        'prev_text' => __('Prev', 'efg'),
        'next_text' => __('Next', 'efg'),
        'type'      => 'plain',
    );

    echo '<nav class="pagination">' . paginate_links ( $page_args ) . '</nav>';
}