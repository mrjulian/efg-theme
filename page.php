<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

get_header();

the_banner();

?>

<div class="content">
    
    <div class="grid">
    
        <div class="container relative">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; ?>

        </div>
        
    </div>
    
</div>
        
<?php get_footer();