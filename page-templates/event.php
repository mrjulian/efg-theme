<?php
/**
 * Template Name: Termin
 *
 * @since       1.0
 */

get_header();

// Get the event outside the loop
$post = $wp_query->post;

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$start_date = tribe_get_start_date( null, false );
$end_date = tribe_get_display_end_date( null, false );

// Display the banner. This must be done after the post has been called.
the_banner();

?>

<div class="content" id="event">

    <?php while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

    <?php endwhile; ?>

</div>

<?php get_footer(); ?>