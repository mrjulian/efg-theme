<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/*--------------------------------------------------------------
 * Seite für Theme settings registrieren
 *------------------------------------------------------------*/

function efg_add_options_page() {
    
    // Setup arguments
    $parent_slug = 'themes.php';
    $page_title = 'Einstellungen';
    $menu_title = 'Einstellungen';
    $capability = 'manage_options';
    $menu_slug  = 'settings';
    $function   = 'efg_render_options_page';
    
    // Register options page
    add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
}

add_action('admin_menu', 'efg_add_options_page');

/*--------------------------------------------------------------
 * Theme settings anzeigen, speichern und laden
 *------------------------------------------------------------*/

function efg_render_options_page() {
    
    // Prüfen, ob Benutzer genügend Rechte besitzt
    if ( ! current_user_can ( 'manage_options' ) ) {
        wp_die('Oops, scheinbar hast du nicht genügend Rechte, um diese Aktion auszuführen.');
    }
    
    // Nonce field überprüfen
    if ( wp_verify_nonce ( '_wp_nonce', 'theme_settings_nonce' ) ) {
        wp_die('Nonce verification failed!');
    }
    
    // Anzahl Beiträge auf Archivseiten speichern
    if ( isset ( $_POST['posts_per_event_page'] ) ) {
        update_option('posts_per_event_page', sanitize_text_field ( $_POST['posts_per_event_page'] ) );
    }
    
    // Anzahl Beiträge auf Archivseiten speichern
    if ( isset ( $_POST['posts_per_archive_page'] ) ) {
        update_option('posts_per_archive_page', sanitize_text_field ( $_POST['posts_per_archive_page'] ) );
    }
    
    // Anzahl der Beitrage auf Suchergebnisseiten speichern
    if ( isset ( $_POST['posts_per_search_page'] ) ) {
        update_option('posts_per_search_page', sanitize_text_field ( $_POST['posts_per_search_page'] ) );
    }
    
    // Anzahl der Beitrage auf Suchergebnisseiten speichern
    if ( isset ( $_POST['sermon_archive_banner'] ) ) {
        update_option('sermon_archive_banner', sanitize_text_field ( $_POST['sermon_archive_banner'] ) );
    }
    
    // Anzahl der Beitrage auf Suchergebnisseiten speichern
    if ( isset ( $_POST['general_archive_banner'] ) ) {
        update_option('general_archive_banner', sanitize_text_field ( $_POST['general_archive_banner'] ) );
    }
    
    // Anzahl der Beitrage auf Suchergebnisseiten speichern
    if ( isset ( $_POST['general_search_banner'] ) ) {
        update_option('general_search_banner', sanitize_text_field ( $_POST['general_search_banner'] ) );
    }
    
    // "CSS above the fold" speichern
    if ( isset ( $_POST['css_above_the_fold'] ) ) {
        update_option('css_above_the_fold', sanitize_text_field ( $_POST['css_above_the_fold'] ) );
    }
    
    include 'settings-form.php';
}