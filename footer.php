<?php

if ( ! defined ( 'ABSPATH' ) ) exit; ?>

<div class="footer">
        
    <div id="accordion" aria-multiselectable="true">

        <div class="card">

            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-service" aria-expanded="true" aria-controls="collapse-service" class="card-header">
                
                <div class="card-title">Gottesdienst</div>
                <div class="card-toggle"><span></span></div> 
                
            </a>

            <div id="collapse" class="collapse" role="tabpanel" aria-labelledby="collapse-service">

                <div class="card-block">

                    <p>Wir feiern jeden Sonntag um 10 Uhr Gottesdienst.</p>
                    <p>Parallel zum Gottesdienst findet ein Kindergottesdienst für Kinder zwischen 4 und 12 Jahren statt.</p>

                </div>

            </div>

        </div>

        <div class="card">

            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-contact" aria-expanded="false" aria-controls="collapse-contact" class="card-header collapsed">
            
                <div class="card-title">Kontakt</div>
                <div class="card-toggle"><span></span></div> 
            
            </a>

            <div id="collapse" class="collapse" role="tabpanel" aria-labelledby="collapse-contact">

                <div class="card-block">

                    <p>So kannst du uns erreichen:</p>

                    <a class="phone" href="tel: +49615133497">Telefon: +49 (6151) 33497</a>
                    <a class="email" href="mailto:buero@efg-darmstadt.de">Mail: buero@efg-darmstadt.de</a>

                </div>

            </div>

        </div>

        <div class="card">

            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-directions" aria-expanded="false" aria-controls="collapse-directions" class="card-header collapsed">
            
                <div class="card-title">Adresse</div>
                <div class="card-toggle"><span></span></div> 
            
            </a>

            <div id="collapse" class="collapse" role="tabpanel" aria-labelledby="collapse-directions">

                <div class="card-block">

                    <p>Hier findest du uns:</p>

                    <div class="address">

                        <p>Ahastraße 12</br>
                        64285 Darmstadt</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

<?php wp_footer(); ?>

</div>

</div>

<div id="menu">
    
    <div class="menu-header">
        
        <div class="container">

            <div class="left">

                <span class="page-title">EFG Darmstadt</span>

            </div>

            <div class="right">

                <div class="close-button">

                    <div class="close-pointer">

                        <div class="close-icon">

                            <span class="first"></span>
                            <span class="second"></span>

                        </div>

                    </div>

                </div>

            </div>
            
        </div>

    </div>
    
    <div class="menu-content">
        
        <div class="links">

            <div class="container">

                <?php

                wp_nav_menu (
                    array (
                        'theme_location' => 'secondary',
                        'menu_id'        => 'first-side-nav',
                        'menu_class'     => 'side-nav',
                        'container'      => 'ul',
                        'walker'         => new efg_main_nav_walker()
                    )
                );

                ?>
                
            </div>

        </div>
        
        <div class="favorites">
            
            <div class="container">
        
                <ul id="secondary-side-nav" class="side-nav">

                    <li class="menu-item"><a href="<?php echo site_url() . '/termine'; ?>">Termine</a></li>
                    <li class="menu-item"><a href="<?php echo site_url() . '/predigten'; ?>">Predigten</a></li>

                </ul>
                
            </div>
        
        </div>
        
        <div class="misc">
            
            <div class="container">
        
                <ul id="third-side-nav" class="side-nav">

                    <li class="menu-item"><a href="<?php echo site_url() . '/kontakt'; ?>">Kontakt</a></li>
                    <li class="menu-item"><a href="<?php echo site_url() . '/kontakt/ansprechpartner'; ?>">Ansprechpartner</a></li>
                    <li class="menu-item"><a href="<?php echo site_url() . '/mitarbeiten'; ?>">Mitarbeiten</a></li>
                    <li class="menu-item"><a href="<?php echo site_url() . '/spenden'; ?>">Spenden</a></li>

                </ul>
                
            </div>
        
        </div>
        
    </div>

</div>

</body>

</html>