/**
 * mobilenav.js
 *
 * Checks viewport and fade divs in.
 *
 */

var jQuery, button, click, scrollTop, page, menu, navbar, windowwidth, cwidth, console, oldwidth;

jQuery(function ($) {

    'use strict';

    // Setup variables
    page = $('#page');
    menu = $('#menu');
    navbar = $('.navbar');
    windowwidth = $(window).width();

    // Return window width to console
    console.log('Current window width: ' + windowwidth + 'px');

    // Navigation animation
    $(document).ready(function ($) {

        // Click navigation button
        $(".toggle-button").live('click', function () {

            // Rotate burger bar
            $('.burger-bar-first').toggleClass('burger-bar-first-active');
            $('.burger-bar-second').toggleClass('burger-bar-second-active');
            $('.burger-bar-third').toggleClass('burger-bar-third-active');

            // Move page to left/right
            if (page.hasClass('menu-closed')) {

                menu.css('opacity', '1');

                if (windowwidth < 768) {
                    page.animate({
                        "left": "-100%"
                    }, 500).removeClass('menu-closed');

                    // Freeze page
                    page.addClass('freeze-page');

                } else if (windowwidth >= 768) {
                    page.animate({
                        "left": "-350px"
                    }, 500).removeClass('menu-closed');
                } else if (windowwidth >= 992) {

                    cwidth = windowwidth - 350;

                    if (navbar.hasClass('navbar-sticky')) {
                        navbar.animate({
                            width: cwidth + 'px'
                        }, 500);
                    }

                    page.animate({
                        "width": cwidth
                    }, 500).removeClass('menu-closed');
                }
            } else {

                setTimeout(
                    function () {
                        menu.css('opacity', '0');
                    }, 500
                );

                if (windowwidth < 768) {
                    page.animate({
                        "left": "0px"
                    }, 500).addClass('menu-closed');

                    // Freeze page
                    page.removeClass('freeze-page');

                } else if (windowwidth >= 768) {
                    page.animate({
                        "left": "0px"
                    }, 500).addClass('menu-closed');
                } else if (windowwidth >= 992) {

                    navbar.animate({
                        width: '100%'
                    }, 500);

                    page.animate({
                        "width": "100%"
                    }, 500).addClass('menu-closed');
                }
            }
        });

        // Click menu close button
        $(".close-button").live('click', function () {

            // Move page to right
            if (!page.hasClass('menu-closed')) {

                setTimeout(
                    function () {
                        menu.css('opacity', '0');
                    }, 500
                );

                if (windowwidth < 768) {
                    page.animate({
                        "left": "0px"
                    }, 500).addClass('menu-closed');

                    // Freeze page
                    page.removeClass('freeze-page');

                } else if (windowwidth >= 768) {
                    page.animate({
                        "left": "0px"
                    }, 500).addClass('menu-closed');
                } else if (windowwidth >= 992) {

                    navbar.animate({
                        width: '100%'
                    }, 500);

                    page.animate({
                        "width": "100%"
                    }, 500).addClass('menu-closed');
                }

                // Reset toggle button
                $('.burger-bar-first').removeClass('burger-bar-first-active');
                $('.burger-bar-second').removeClass('burger-bar-second-active');
                $('.burger-bar-third').removeClass('burger-bar-third-active');
            }
        });

        // Click on submenu
        $('.menu-item-inline').click(function () {

            // Find clicked element
            var par = $(this).parents('.menu-item');

            // Animate submenu
            par.toggleClass('sub-menu-open');
            par.find('.vertical-line').toggleClass('vertical-line-rotate');
            par.find('.sub-menu').slideToggle();

            return false;
        });
    });

    // Sticky navbar on scroll
    $(window).on('scroll', function () {

        // Redetect scroll position
        scrollTop = $(window).scrollTop();
        oldwidth = windowwidth;

        if (scrollTop > 430) {

            // Do nothing if nav is opened
            if (!$('.main-nav').hasClass('active')) {
                $(".navbar").addClass("navbar-sticky");
                $(".default-nav").addClass("sticky-nav");
            }
        } else {

            $(".navbar").removeClass("navbar-sticky");
            $(".default-nav").removeClass("sticky-nav");
        }
    });
});